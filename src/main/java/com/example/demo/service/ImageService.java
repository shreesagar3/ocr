package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.ImageData;
import com.example.demo.repository.ImageRepository;

@Service
public class ImageService {
   
	@Autowired
	private ImageRepository imageRepository;
	//Create operation
	public ImageData create(String filename,String imagedata,String imagepath) {
		return imageRepository.save(new ImageData(filename, imagedata,imagepath));
	}
	//Retrieve operation
	public List<ImageData> getAll(){
		return imageRepository.findAll();
	}
	public ImageData getByFilename(String filename) {
		return imageRepository.findByFilename(filename);
	}
}
