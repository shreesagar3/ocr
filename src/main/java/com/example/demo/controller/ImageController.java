package com.example.demo.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.ImageData;
import com.example.demo.service.ImageService;

import net.sourceforge.tess4j.*;


@RestController

public class ImageController {

	@Autowired
	private ImageService imageService;
	
	@RequestMapping("/create")
	@PostMapping
	public ImageData create(@RequestParam String filepath,@RequestParam String filename) throws Exception {
		//@RequestParam(name="file") MultipartFile file
		String result = "";
		ImageData p;
		try {
			System.out.println("Request with filename "+filename+" with path "+filepath+" recieved");
			File file = new File(filepath+filename);
			BufferedImage img = ImageIO.read(file);
			Tesseract tesseract = new Tesseract();
			//ITesseract tesseract = new Tesseract();
			tesseract.setLanguage("eng");
			//Please set datapath of tessdata folder
	        // tesseract.setDatapath("/home/hackerman/Documents/workspace-spring-tool-suite-4-4.5.1.RELEASE/tessdata");
			tesseract.setDatapath("src/main/resources/tessdata");
	        result = tesseract.doOCR(img);
		}catch(IOException e) {
			throw new Exception("Error occured while tesseract image extraction\n "+e.getLocalizedMessage());
		}
		p = imageService.create(filename, result, filepath);
        return p;
	}
	
	@RequestMapping("/get")
	public ImageData getImage(@RequestParam String filename) {
		return imageService.getByFilename(filename);
	}
	@RequestMapping("/getall")
	public List<ImageData> getAll(){
		return imageService.getAll();
	}

}
