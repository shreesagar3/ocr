package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ImageData {
	@Id
	String id;
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getImagedata() {
		return imagedata;
	}
	public void setImagedata(String imagedata) {
		this.imagedata = imagedata;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public ImageData(String filename, String imagedata, String filepath) {
		this.filename = filename;
		this.imagedata = imagedata;
		this.filepath = filepath;
	}
	String filename;
	String imagedata;
	String filepath;
	public String toString() {
		return "ImagePath: "+filepath+"\nImageData:\n"+imagedata;
	}
}
