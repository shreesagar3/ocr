package com.example.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.ImageData;

@Repository
public interface ImageRepository extends MongoRepository<ImageData, String>{
	public ImageData findByFilename(String filename);
}
