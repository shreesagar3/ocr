package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTess4jMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTess4jMongoApplication.class, args);
	}
}
